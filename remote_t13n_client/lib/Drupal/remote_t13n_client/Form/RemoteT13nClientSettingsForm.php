<?php
/**
 * @file
 * Contains \Drupal\t13n_client\Form\t13nClientSettingsForm.
 *
 * @todo Validate URLs; make sure there's an accessible site there.
 */

namespace Drupal\remote_t13n_client\Form;

use Drupal\Core\Form\ConfigFormBase;

class RemoteT13nClientSettingsForm extends ConfigFormBase {
  
  /**
   * @{inheritdoc}
   */
  public function getFormID() {
    return 'remote_t13n_client_settings_form';
  }

  /**
   * @{inheritdoc}
   */
  public function buildForm(array $form, array &$form_state) {
    $config = $this->configFactory->get('remote_t13n_client.settings');

    $form['server_url'] = array(
      '#type' => 'url',
      '#title' => $this->t('Remote server URL'),
      '#description' => $this->t('The root URL of the server Drupal installation.'),
      '#required' => TRUE,
      '#default_value' => $config->get('server.url'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * @{inheritdoc}
   */
  public function submitForm(array &$form, array &$form_state) {
    $config = $this->configFactory->get('remote_t13n_client.settings');
    $config->set('server.url', $form_state['values']['server_url'])->save();

    parent::submitForm($form, $form_state);
  }
}
