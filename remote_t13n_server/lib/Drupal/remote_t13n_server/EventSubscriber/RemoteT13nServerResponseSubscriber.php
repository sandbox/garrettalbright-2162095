<?php
/**
 * @file
 * Provides the RemoteT13nServerResponseSubscriber class.
 */
namespace Drupal\remote_t13n_server\EventSubscriber;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RemoteT13nServerResponseSubscriber implements EventSubscriberInterface {

  /**
   * onKernelResponse event.
   */
  public function onKernelResponse(FilterResponseEvent $event) {
    // Wa the request for the machine name transliteration path?
    // @todo Chceking against the path seems wrong here. Is there a better way?
    if ($event->getRequestType() !== HttpKernelInterface::MASTER_REQUEST || $event->getRequest()->getPathInfo() !== '/machine_name/transliterate') {
      return;
    }
    // Add a global Access-Control-Allow-Origin header.
    // @see http://en.wikipedia.org/wiki/Cross-origin_resource_sharing
    $event->getResponse()->headers->set('Access-Control-Allow-Origin', '*');
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = array('onKernelResponse');
    return $events;
  }

}
