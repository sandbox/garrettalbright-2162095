(function ($, Drupal, drupalSettings) {

"use strict";

Drupal.behaviors.machineName.transliterate = function (source, settings) {
  // serverUrl will be null if it hasn't been set yet.
  var serverUrl = drupalSettings.remoteT13nClient.serverUrl !== null ? drupalSettings.remoteT13nClient.serverUrl : drupalSettings.path.basePath;
  return $.get(serverUrl + 'machine_name/transliterate', {
    text: source,
    langcode: drupalSettings.langcode,
    replace_pattern: settings.replace_pattern,
    replace: settings.replace,
    lowercase: true
  });
};

})(jQuery, Drupal, drupalSettings);
