<?php
/**
 * @file
 * Contains \Drupal\t13n_client\Transliteration\RemoteT13nClient.
 */

namespace Drupal\remote_t13n_client\Transliteration;

use Drupal\Core\Transliteration\PHPTransliteration;
use GuzzleHttp\Client;
use Guzzle\Common\Exception;

/**
 * The T13nClient class.
 */
class RemoteT13nClient extends PHPTransliteration {

  /**
   * @{inheritdoc}
   */
  public function transliterate($string, $langcode = 'en', $unknown_character = '?', $max_length = NULL) {
    $server_url = \Drupal::config('remote_t13n_client.settings')->get('server.url');
    // Pass through if there's no server URL
    if (empty($server_url)) {
      watchdog('t13n_client', 'The server URL has not been set for the Transliteration Client module. Passing through to the default transliteration system.', NULL, WATCHDOG_ERROR);
    }
    else {
      $guzzle = new Client();
      $response = $guzzle->get($server_url . '/machine_name/transliterate', array(
        'query' => array(
          'langcode' => $langcode,
          'replace' => $unknown_character,
          'text' => $string,
        ),
        // @todo Make this user-modifiable?
        'timeout' => 1,
        // Don't thrown an exception if things fail.
        'exceptions' => FALSE,
      ));
      $status_code = $response->getStatusCode();
      if ($status_code == 200) {
        try {
          $transliterated_string = $response->json();
          // We can safely use substr() here since $transliterated_string should be
          // all ASCII at this point.
          return $max_length === NULL ? $transliterated_string : substr($transliterated_string, 0, $max_length);
        }
        catch (\RuntimeException $e) {
          watchdog('t13n_client', 'Parsing the response from the server failed. The response body was: "@response". Passing through to the default transliteration system.', array('@response' => substr((string)$response->getBody(), 0, 255)), WATCHDOG_ERROR);
        }
      }
      else {
        // Rats.
        watchdog('t13n_client', 'Transliteration Client could not properly contact the server. The HTTP status code was @code. Passing through to the default transliteration system.', array('@code' => $status_code), WATCHDOG_ERROR);
      }
    }
    // If we made it here, things have failed. Pass through to the default system.
    return parent::transliterate($string, $langcode, $unknown_character, $max_length);
  }
  
}
